from app import app
from flask import render_template
@app.route('/')
def index():
    return render_template('mainpage.html')

@app.route('/authtorization')
def auth():
    return render_template('authtorization.html')

@app.route('/api/link_cut')
def cutter():
    return 